# DCI Berlin JavaScript WORKSHOPS

## Students

Check out the [Slides](https://irena.github.io/js-/#/)

### Slides as PDF

If you want to export the slides as a PDF file, please visit this link instead:



Then, choose "File - Print" in your browser and choose "Save as PDF".

## Teachers

Check out the teacher's [README](README-teachers.md)
