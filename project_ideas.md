# Plant App

See [final_project/README.md](final_project/README.md)

# Project ideas:

## Find non-smoking bars


## International Weather app

## eLearning website

* Show a series of videos on user's choice
* Could be academic or practical life tips

## Multilanguage dictionary

* Translate a word simultaneously into multiple languages
* Useful when meeting with a multilingual crowd



## Movies and TV Series app

* Search for movies and TV
* Show information and trailers



